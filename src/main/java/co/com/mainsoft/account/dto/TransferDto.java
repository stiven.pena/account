package co.com.mainsoft.account.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferDto {

    private String numberAccountSender;
    private String typeAccountSender;

    private String numberAccountReceiver;
    private String typeAccountReceiver;

    private Integer amountTransfer;

}
