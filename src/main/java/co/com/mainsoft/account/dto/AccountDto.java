package co.com.mainsoft.account.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDto {

    private Integer idAccount;
    private String number;
    private String type;
    private Integer amountAccount;
}
