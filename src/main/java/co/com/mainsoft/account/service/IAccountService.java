package co.com.mainsoft.account.service;

import co.com.mainsoft.account.dto.AccountDto;
import co.com.mainsoft.account.dto.ResponseGeneric;
import co.com.mainsoft.account.dto.TransferDto;

import java.util.List;

public interface IAccountService {

    List<AccountDto> findAll();

    AccountDto findById(Integer id) throws Exception;

    void create(AccountDto accountDto) throws Exception;

    AccountDto findByTypeAccountAndNumberAccount(String typeAccount, String numberAccount) throws Exception;

    ResponseGeneric transferCash(TransferDto transferDto);

}
