package co.com.mainsoft.account.service.impl;

import co.com.mainsoft.account.dto.AccountDto;
import co.com.mainsoft.account.dto.ResponseGeneric;
import co.com.mainsoft.account.dto.TransferDto;
import co.com.mainsoft.account.entity.AccountEntity;
import co.com.mainsoft.account.repository.IAccountRepository;
import co.com.mainsoft.account.service.IAccountService;
import co.com.mainsoft.account.util.AccountUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements IAccountService {

    @Autowired
    IAccountRepository iAccountRepository;

    @Override
    public List<AccountDto> findAll() {

        List<AccountEntity> accountEntityList = iAccountRepository.findAll();
        List<AccountDto> productDtoList = new ArrayList<>();

        for (AccountEntity entity : accountEntityList) {
            productDtoList.add(AccountUtil.accountEntityToAccountDto(entity));
        }
        return productDtoList;
    }


    @Override
    public AccountDto findById(Integer id) throws Exception {

        Optional<AccountEntity> accountEntity = iAccountRepository.findById(id);
        if (accountEntity.isPresent())
            return AccountUtil.accountEntityToAccountDto(accountEntity.get());
        else
            throw new Exception("La cuenta ya existe");
    }

    @Override
    public void create(AccountDto accountDto) throws Exception {
        Optional<AccountEntity> accountEntity
                = iAccountRepository.findByTypeAccountAndNumberAccount(accountDto.getType(), accountDto.getNumber());

        if (!accountEntity.isPresent())
            iAccountRepository.save(AccountUtil.accountDtoToAccountEntity(accountDto));
        else
            throw new Exception("La cuenta ya existe");

    }

    @Override
    public AccountDto findByTypeAccountAndNumberAccount(String typeAccount, String numberAccount) throws Exception {

        Optional<AccountEntity> accountEntity
                = iAccountRepository.findByTypeAccountAndNumberAccount(typeAccount, numberAccount);

        if (!accountEntity.isPresent())
            return AccountUtil.accountEntityToAccountDto(accountEntity.get());
        else
            throw new Exception("La cuenta No existe");

    }


    @Override
    public ResponseGeneric transferCash(TransferDto transferDto) {
        ResponseGeneric responseGeneric = new ResponseGeneric();

        Optional<AccountEntity> accountSender =
                iAccountRepository.findByTypeAccountAndNumberAccount(transferDto.getTypeAccountSender(), transferDto.getNumberAccountSender());
        Optional<AccountEntity> accountReceiver
                = iAccountRepository.findByTypeAccountAndNumberAccount(transferDto.getTypeAccountReceiver(), transferDto.getNumberAccountReceiver());

        if (accountReceiver.isEmpty() || accountSender.isEmpty()) {
            responseGeneric.setCode("206");
            responseGeneric.setMessage("Una de las cuentas no es correcta");

        } else {
            if (accountSender.get().getAmount() >= transferDto.getAmountTransfer()) {

                accountSender.get().setAmount(accountSender.get().getAmount() - transferDto.getAmountTransfer());
                accountReceiver.get().setAmount( accountReceiver.get().getAmount() + transferDto.getAmountTransfer());

                iAccountRepository.save(accountReceiver.get());
                iAccountRepository.save(accountSender.get());

                responseGeneric.setCode("200");
                responseGeneric.setMessage("Transaccion Exitosa");
            } else {
                responseGeneric.setCode("206");
                responseGeneric.setMessage("Monto Insuficiente");
            }

        }
        return responseGeneric;
    }


}
