package co.com.mainsoft.account.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "ACCOUNT")
public class AccountEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "NUMBER_ACCOUNT")
    private String numberAccount;

    @Column(name = "TYPE_ACCOUNT")
    private String typeAccount;

    @Column(name = "AMMOUNT")
    private Integer amount;

    @Column(name = "STATUS")
    private Integer status;

}
