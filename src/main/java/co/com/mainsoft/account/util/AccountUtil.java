package co.com.mainsoft.account.util;

import co.com.mainsoft.account.dto.AccountDto;
import co.com.mainsoft.account.entity.AccountEntity;

public class AccountUtil {

    public static AccountDto accountEntityToAccountDto(AccountEntity entity){
        return  AccountDto.builder()
                .idAccount(entity.getId())
                .amountAccount(entity.getAmount())
                .number(entity.getNumberAccount())
                .type(entity.getTypeAccount())

                .build();
    }

    public static AccountEntity accountDtoToAccountEntity(AccountDto dto){
        return  AccountEntity.builder()
                .amount(dto.getAmountAccount())
                .numberAccount(dto.getNumber())
                .typeAccount(dto.getType())
                .status(1)
                .build();
    }

    public static AccountEntity accountEntityToAccountEntity(AccountEntity accountEntity){
        return  AccountEntity.builder()
                .amount(accountEntity.getAmount())
                .numberAccount(accountEntity.getNumberAccount())
                .typeAccount(accountEntity.getTypeAccount())
                .status(accountEntity.getStatus())
                .build();
    }

}
