package co.com.mainsoft.account.controller;

import co.com.mainsoft.account.dto.AccountDto;
import co.com.mainsoft.account.dto.TransferDto;
import co.com.mainsoft.account.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    IAccountService iAccountService;

    @GetMapping()
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(iAccountService.findAll());
    }

    @PostMapping()
    public ResponseEntity<?> create(@RequestBody AccountDto accountDto) {
        try {
            iAccountService.create(accountDto);
            return new ResponseEntity<>(HttpStatus.CREATED);

        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.CONFLICT);
        }
    }



    @PostMapping("/transfer")
    public ResponseEntity<?> transferCash(@RequestBody TransferDto transferDto) {

        return new ResponseEntity<>(iAccountService.transferCash(transferDto), HttpStatus.OK);

    }
}
