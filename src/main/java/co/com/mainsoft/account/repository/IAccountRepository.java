package co.com.mainsoft.account.repository;

import co.com.mainsoft.account.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface IAccountRepository extends JpaRepository<AccountEntity, Integer> {


    @Override
    @Query("SELECT account FROM AccountEntity account WHERE  account.status <> 0")
    List<AccountEntity> findAll();

    @Override
    @Query("SELECT account FROM AccountEntity account WHERE  account.id = :id AND account.status <> 0")
    Optional<AccountEntity> findById(Integer id);

    @Query("SELECT account FROM AccountEntity account WHERE  account.typeAccount = :typeAccount AND account.numberAccount= :numberAccount AND account.status <> 0")
    Optional<AccountEntity> findByTypeAccountAndNumberAccount(String typeAccount, String numberAccount);

}
